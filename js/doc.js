function hideControls() {
  document.querySelector('body').classList.remove('open')
}

function hideMenu() {
  document.querySelector('body > header').classList.remove('opened')
}

document.addEventListener('DOMContentLoaded', function() {
  /*** ouverture / fermeture de la sidebar au clic sur l'icône sidebar-control ***/
  document.querySelectorAll('.sidebar-control').forEach(function(item) {
    item.addEventListener('click', function() {
      document.querySelector('body').classList.toggle('open')
      /* on cache aussi le menu si il est ouvert */
      hideMenu()
    })
  })
  /*** fermeture de la sidebar au clic sur un lien de la sidebar ***/
  document.querySelectorAll('#control-panel a').forEach(function(item) {
    item.addEventListener('click', function() {
      hideControls()
    })
  })
  /*** fermeture de la sidebar au clic sur le contenu principal encore visible ***/
  document.querySelector('main > .dw-content').addEventListener('click', function() {
    hideControls()
  })
  /*** virer le toggle natif de la TOC ***/
  let tocTitle = document.querySelector('#dw__toc > h3')
  if (tocTitle) {
    let tocTitleClone = tocTitle.cloneNode(true)
    document.querySelector('#dw__toc').replaceChild(tocTitleClone, tocTitle)
  }
  /*** désactive l'autocompletion de la recherche qui masque les pages trouvées ***/
  document.querySelector('#qsearch__in').setAttribute('autocomplete','off')

  /*** désactive le higlight de section (qui fait sauter les marges des titres) ***/
  jQuery('form.btn_secedit').unbind('mouseover')
  setTimeout(function(){
    jQuery('form.btn_secedit').unbind('mouseover')
  }, 1000)

  /*** retire l'affichage du nom complet (identique au nom d'utilisateur) ***/
  let htmlUserInfo = document.querySelector('#controls .user').innerHTML
  htmlUserInfo = htmlUserInfo.substring(0, htmlUserInfo.indexOf('</bdi>')+6)
  document.querySelector('#controls .user').innerHTML = htmlUserInfo
})
