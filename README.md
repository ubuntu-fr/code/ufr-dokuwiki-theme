# Thème Dokuwiki pour [doc.ubuntu-fr.org](https://doc.ubuntu-fr.org/)

- Ce thème dépend de [ufr-cms](https://gitlab.com/ubuntu-fr/code/ufr-cms), qui inclut des assets généraux (images, CSS, JavaScript) nécessaires à la bonne présentation de la documentation.
- Basé sur [Dokuwiki Template Starter](https://github.com/selfthinker/dokuwiki_template_starter).

## Développement

### Environnement

- L'environnement de développement peut se déployer en quelques clics depuis [ufr-dev-env](https://gitlab.com/ubuntu-fr/code/ufr-dev-env) (répertoire du thème : `ufr-doc/dokuwiki/dokuwiki/lib/tpl/ubuntu`).
- Voir aussi éventuellement [ufr-doc](https://gitlab.com/ubuntu-fr/code/ufr-doc) pour l'instance DokuWiki.

### Guide

La mise en forme générale est à établir sur [ufr-cms](https://gitlab.com/ubuntu-fr/code/ufr-cms).  
Seuls les styles et directives spécifiques à DokuWiki (éléments de l'interface, styles sur des classes propres à DokuWiki) sont à rédiger dans les fichiers `css/doc.css` et `js/doc.js`.

#### CSS

Les regions `#region` / `#endregion` permettent de structurer le fichier.  
On peut replier chaque region dans VS Code, et accéder rapidement à une région avec le plugin _Region Viewer_.
