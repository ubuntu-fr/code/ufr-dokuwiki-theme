<?php
/**
 * DokuWiki Ubuntu Template
 *
 * @link     https://gitlab.com/ubuntu-fr/code/ufr-dokuwiki-theme
 * @author   krodelabestiole
 * @license  MIT
 */

if (!defined('DOKU_INC')) die(); /* must be run from within DokuWiki */
@require_once(dirname(__FILE__).'/tpl_functions.php'); /* include hook for template functions */
header('X-UA-Compatible: IE=edge,chrome=1');

$showTools = !tpl_getConf('hideTools') || ( tpl_getConf('hideTools') && !empty($_SERVER['REMOTE_USER']) );
$showSidebar = page_findnearest($conf['sidebar']) && ($ACT=='show');

$domain = $_SERVER['HTTP_HOST'];
if (stripos($domain,'localhost') !== false) { // développement
	$main = '//ufr-cms.localhost';
}
elseif (stripos($domain,'crachecode') !== false) { // preview
	$main = '//www.ubuntu-fr.org';
}
else { // production
	$main = '//www.ubuntu-fr.org';
}

?><!DOCTYPE html>
<html lang="<?php echo $conf['lang'] ?>" dir="<?php echo $lang['direction'] ?>">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="<?php echo $main ?>/favicon.png">
	<meta property="og:type" content="website">

	<?php tpl_metaheaders(false) ?>

	<title><?php tpl_pagetitle() ?> [<?php echo strip_tags($conf['title']) ?>]</title>
	<meta property="og:image" content="{% if page.og-image %}{{ page.og-image }}{% else %}{{ site.og-image }}{% endif %}">
	<meta name="theme-color" content="#5E2750">
	
	<link rel="stylesheet" href="<?php echo $main ?>/css/main.css">
	<link rel="stylesheet" href="/lib/tpl/ubuntu/css/doc.css">
  <link rel="stylesheet" href="<?php echo $main ?>/css/print.css" media="print">

	<script type="module" src="<?php echo $main ?>/js/app.js" defer></script>
	<script type="module" src="/lib/tpl/ubuntu/js/doc.js" defer></script>
	
</head>

<?php /*

tpl_classes() provides useful CSS classes;
if you choose not to use it, the 'dokuwiki' class at least
should always be in one of the surrounding elements
(e.g. plugins and templates depend on it) 

the "dokuwiki__top" id is needed somewhere at the top,
because that's where the "back to top" button/link links to

*/ ?>
<body id="dokuwiki__top" class="dokuwiki">
  <header>
    <div class="topbar">
      <a href="./" id="logo">
        <img src="<?php echo $main ?>/img/logo.svg">
			</a>
			<div class="sidebar-control"></div>
      <div id="sandwich">
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
    <div class="navbar">
      <nav>
				<?php
					$domain = $_SERVER['HTTP_HOST'];
					if (stripos($domain,'localhost') !== false) { // développement
				?>
					<a href="//ufr-cms.localhost/">Accueil</a>
					<a href="/" class="active">Documentation</a>
					<a href="//ufr-forum.localhost/">Forum</a>
					<a href="//ufr-cms.localhost/about/">À propos</a>
				<?php
					}
					elseif (stripos($domain,'crachecode') !== false) { // preview
				?>
					<a href="//www.ubuntu-fr.org/">Accueil</a>
					<a href="/" class="active">Documentation</a>
					<a href="//ufr-forum.crachecode.net/">Forum</a>
					<a href="//www.ubuntu-fr.org/about/">À propos</a>
				<?php
					}
					else { // production
				?>
					<a href="//www.ubuntu-fr.org/">Accueil</a>
					<a href="/" class="active">Documentation</a>
					<a href="//forum.ubuntu-fr.org/">Forum</a>
					<a href="//www.ubuntu-fr.org/about/">À propos</a>
				<?php
					}
				?>
			</nav>
      <div class="switchers">
        <div class="switcher" id="switcher-wide"><div></div><div></div></div>
        <div href="#" class="switcher" id="switcher-light"></div>
        <div href="#" class="switcher" id="switcher-dark"></div>
				<div class="sidebar-control"></div>
      </div>
    </div>
  </header>
	<main id="<?php echo $INFO['id']; ?>">

		<!-- ********** CONTENU ********** -->
		<div id="dokuwiki__content" class="dw-content">
			<div class="pad">
				<?php tpl_flush() /* flush the output buffer */ ?>

				<div class="page">
					<!-- wikipage start -->
					<?php tpl_content(false) /* the main content */ ?>
					<!-- wikipage stop -->
					<div class="clearer"></div>
				</div>

				<?php tpl_flush() ?>
				<?php tpl_includeFile('pagefooter.html') ?>
			</div>
			<!-- ********** FOOTER ********** -->
			<div id="dokuwiki__footer">
				<div class="doc"><?php tpl_pageinfo() /* 'Last modified' etc */ ?></div>
				<?php tpl_license('button') /* content license, parameters: img=*badge|button|0, imgonly=*0|1, return=*0|1 */ ?>
			</div><!-- /footer -->
			<?php tpl_includeFile('footer.html') ?>
		</div><!-- /content -->

		<!-- ********** SIDEBAR ********** -->
		<div id="control-panel">
			<div>
					
				<!-- ********** CHAMPS DE RECHERCHE ********** -->
				<?php tpl_searchform() ?>

				<div id="controls">
					<!-- ********** CONNEXION ********** -->
					<section>
					<?php if ($conf['useacl'] && $showTools): ?>
						<ul>
							<?php
							if (!empty($_SERVER['REMOTE_USER'])) {
								echo '<li class="user">';
								tpl_userinfo(); /* 'Logged in as ...' */
								echo '</li>';
							}
							?>
							<?php /* the optional second parameter of tpl_action() switches between a link and a button,
									e.g. a button inside a <li> would be: tpl_action('edit', 0, 'li') */
							?>
							<?php tpl_toolsevent('usertools', array(
								'register'  => tpl_action('register', 1, 'li', 1),
								'login'     => tpl_action('login', 1, 'li', 1),
							)); ?>
						</ul>
					<?php endif ?>
					</section>

					<!-- ********** MESSAGES ********** -->
					<section>
						<?php html_msgarea() /* occasional error and info messages on top of the page */ ?>
					</section>

					<!-- ********** TABLE DES MATIÈRES ********** -->
					<section id="sec_toc">
						<?php tpl_toc() ?>
					</section>
					
					<!-- ********** OUTILS UTILISATEURS ********** -->
					<?php if ($conf['useacl'] && $showTools): ?>
						<section>
							<h3><?php echo $lang['user_tools'] ?></h3>
							<ul>
								<?php /* the optional second parameter of tpl_action() switches between a link and a button,
										e.g. a button inside a <li> would be: tpl_action('edit', 0, 'li') */
								?>
								<?php
								if ($INFO['isadmin']) {
									tpl_toolsevent('usertools', array(
										'admin'     => tpl_action('admin', 1, 'li', 1),
									));
								}
								?>
								<?php tpl_toolsevent('usertools', array(
									'userpage'  => _tpl_action('userpage', 1, 'li', 1),
									'profile'   => tpl_action('profile', 1, 'li', 1),
								)); ?>
							</ul>
						</section>
					<?php endif ?>

					<!-- ********** OUTILS SITE ********** -->
					<section>
						<h3><?php echo $lang['site_tools'] ?></h3>
						<ul>
							<?php tpl_toolsevent('sitetools', array(
								'recent'    => tpl_action('recent', 1, 'li', 1),
								'media'     => tpl_action('media', 1, 'li', 1),
								'index'     => tpl_action('index', 1, 'li', 1),
							)); ?>
						</ul>
					</section>

					<!-- ********** OUTILS PAGE ********** -->
					<?php if ($showTools): ?>
						<section>
							<h3><?php echo $lang['page_tools'] ?></h3>
							<ul>
								<?php tpl_toolsevent('pagetools', array(
									'edit'      => tpl_action('edit', 1, 'li', 1),
									'discussion'=> _tpl_action('discussion', 1, 'li', 1),
									'revisions' => tpl_action('revisions', 1, 'li', 1),
									'backlink'  => tpl_action('backlink', 1, 'li', 1),
									'subscribe' => tpl_action('subscribe', 1, 'li', 1),
									'revert'    => tpl_action('revert', 1, 'li', 1),
								)); ?>
							</ul>
						</section>
					<?php endif; ?>

					<!-- ********** DIVERS ********** -->
					<section>
						<h3>Divers</h3>
							<ul>
								<li><a href="/wiki/participer_wiki">Participer à la documentation</a></li>
								<li><a href="/documentation_hors_ligne">Documentation hors ligne</a></li>
								<li><a href="https://www.ubuntu-fr.org/download/">Télécharger Ubuntu</a></li>
							</ul>
					</section>
				</div>
			</div>
		</div>
    	<div class="no"><?php tpl_indexerWebBug() /* provide DokuWiki housekeeping, required in all templates */ ?></div>
	</main>
	
	<!-- Piwik -->
	<script type="text/javascript">
		  var _paq = _paq || [];
		  _paq.push(["setDomains", ["*.doc.ubuntu-fr.org","*.doc.edubuntu-fr.org","*.doc.lubuntu-fr.org","*.doc.xubuntu-fr.org","*.doc.edubuntu-fr.org","*.doc.lubuntu-fr.org","*.doc.ubuntu-fr.org","*.doc.xubuntu-fr.org"]]);
		  _paq.push(['trackPageView']);
		  _paq.push(['enableLinkTracking']);
		  (function() {
		    var u="//piwik.ubuntu-fr.org/";
		    _paq.push(['setTrackerUrl', u+'piwik.php']);
		    _paq.push(['setSiteId', 3]);
		    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
		  })();
		</script>
		<noscript><p><img src="//piwik.ubuntu-fr.org/piwik.php?idsite=3" style="border:0;" alt="" /></p></noscript>
	<!-- End Piwik Code -->

</body>
</html>
